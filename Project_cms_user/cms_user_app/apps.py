from django.apps import AppConfig


class CmsUsetAppConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "cms_user_app"
