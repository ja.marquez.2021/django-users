from django.urls import path

from . import views

urlpatterns = [
    path('', views.index,name="index"),
    path('logout', views.log_out),
    path('log_in', views.log_in,name="log_in"),
    path('reset', views.reset,name="reset"),
    path("<name>", views.content),

    ]