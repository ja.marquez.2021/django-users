from django.db import models

class Content(models.Model):
    """
    En esta base de datos se almacenan los contenidos
    name = nombre del contenido
    value = valor del contenido
    """

    name = models.CharField(max_length=264)
    value = models.TextField()