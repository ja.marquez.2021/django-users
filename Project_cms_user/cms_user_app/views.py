from django.contrib.auth import logout
from django.http import HttpResponse
from django.shortcuts import redirect
from django.template import loader
from django.views.decorators.csrf import csrf_exempt

from .models import Content


def index(request):
    template = loader.get_template('index_page.html')
    context = {'content': Content.objects.all(),
               'user': request.user}
    return HttpResponse(template.render(context, request))


@csrf_exempt
def content(request, name):
    template = loader.get_template('pantilla_respuesta.html')
    context = {'user': request.user}
    if request.method == "GET":
        try:
            contenido = Content.objects.get(name=name).value
            context.update({"respuesta":
                            f"El contenido correspondiente a {name} "
                            f"es: {contenido}"})
        except Content.DoesNotExist:
            context.update({"respuesta": f"El contenido con nombre {name} "
                                         f"no existe."})
    elif request.method == "PUT" and request.user.is_authenticated:
        body = request.body.decode("utf-8")
        if body and not Content.objects.filter(name=name).exists():
            contenido = Content(name=name, value=body)
            contenido.save()
            context.update({"respuesta":
                            f"Guardando un contenido con "
                            f"nombre {name} cullo cuerpo es: {body}"})
        else:
            context.update({"respuesta":
                            f"El contenido con nombre {name} "
                            f"ya esta asignado."})
    elif not request.user.is_authenticated:
        context.update({"respuesta":
                        "No puedes hacer esto, no estas autenticado, "
                        "logeate por favor."})

    return HttpResponse(template.render(context, request))


def log_out(request):
    logout(request)
    return redirect("index")


def log_in(request):
    return redirect("admin/")


def reset(request):
    Content.objects.all().delete()
    return redirect("index")
